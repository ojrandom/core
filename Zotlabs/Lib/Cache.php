<?php /** @file */

namespace Zotlabs\Lib;

use Zotlabs\Lib\Config;

/**
 *  cache api
 */
class Cache {

    /**
     * @brief Returns cached content
     *
     * @param string $key
     * @param string $age in SQL format, default is '30 DAY'
     * @return string
     */

	public static function get($key, $age = '') {
//		$hash = hash('whirlpool',$key);
		$hash = uuid_from_url($key);

		$r = q("SELECT v FROM cache WHERE k = '%s' AND updated > %s - INTERVAL %s LIMIT 1",
			dbesc($hash),
			db_utcnow(),
			db_quoteinterval(($age ? $age : Config::Get('system','object_cache_days', '30') . ' DAY'))
		);

		if ($r)
			return $r[0]['v'];
		return null;
	}

	public static function set($key,$value) {
//		$hash = hash('whirlpool',$key);
		$hash = uuid_from_url($key);

		$r = q("SELECT * FROM cache WHERE k = '%s' LIMIT 1",
			dbesc($hash)
		);
		if($r) {
			q("UPDATE cache SET v = '%s', updated = '%s' WHERE k = '%s'",
				dbesc($value),
				dbesc(datetime_convert()),
				dbesc($hash)
			);
		}
		else {
			q("INSERT INTO cache (k, v, updated) VALUES ('%s', '%s', '%s')",
				dbesc($hash),
				dbesc($value),
				dbesc(datetime_convert())
			);
		}
	}
}
