<?php

/**
 *   * Name: Redbasic
 *   * Description: Hubzilla standard theme
 *   * Version: 2.2
 *   * MinVersion: 8.9
 *   * MaxVersion: 11.0
 *   * Author: Fabrixxm
 *   * Maintainer: Mike Macgirvin
 *   * Maintainer: Mario Vavti
 *   * Theme_Color: rgb(248, 249, 250)
 *   * Background_Color: rgb(254,254,254)
 */


function redbasic_init() {

}
