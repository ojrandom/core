
Estimado/a {{$username}},
	Recientemente se ha recibido una solicitud en {{$sitename}} para restablecer su cuenta. 
contraseña. Para confirmar esta solicitud, seleccione el enlace de verificación
a continuación o péguelo en la barra de direcciones de su navegador web. 

Si NO ha solicitado este cambio, NO siga el enlace
proporcionado e ignore y/o elimine este correo electrónico. 

Su contraseña no se cambiará a menos que podamos verificar que usted 
emitió esta solicitud. 

Siga este enlace para verificar su identidad: 

{{$reset_link}}

A continuación, recibirá un mensaje de seguimiento con la nueva contraseña.

Puede cambiar esa contraseña desde la página de configuración de su cuenta después de iniciar sesión.

Los datos de acceso son los siguientes: 

Ubicación del sitio:{{$siteurl}} 
Nombre de acceso:	{{$email}}




Atentamente, 
	{{$sitename}} Administrador

--
Términos del servicio
{{$siteurl}}/help/TermsOfService
 
